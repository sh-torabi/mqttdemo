﻿using MQTTnet;
using MQTTnet.Server;
using System.Text;

namespace MQTTServerDemo;

public class Server : IHostedService, IDisposable
{
    private readonly MqttServer _mqttServer;
    private readonly ILogger<Server> _logger;
    private bool _disposedValue;

    public Server(ILogger<Server> logger)
    {
        _logger = logger;
        var mqttFactory = new MqttFactory();
        var mqttServerOptions = mqttFactory.CreateServerOptionsBuilder()
            .WithDefaultEndpoint()
            .WithDefaultEndpointPort(704)
            .Build();

        _mqttServer = mqttFactory.CreateMqttServer(mqttServerOptions);
        _mqttServer.ValidatingConnectionAsync += OnNewConnection;
        _mqttServer.InterceptingPublishAsync += OnNewMessage;

        _logger = logger;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Start MQTT Server");
        await _mqttServer.StartAsync();
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Stop MQTT Server");
        await _mqttServer.StopAsync();
    }

    public Task OnNewConnection(ValidatingConnectionEventArgs context)
    {
        _logger.LogInformation(
                $"New connection: ClientId = {context.ClientId}, Endpoint = {context.Endpoint}, CleanSession = {context.CleanSession}");
        return Task.CompletedTask;
    }

    public Task OnNewMessage(InterceptingPublishEventArgs context)
    {
        var payload = context.ApplicationMessage?.Payload == null ? null : Encoding.UTF8.GetString(context.ApplicationMessage?.Payload);

        _logger.LogInformation(
            $"TimeStamp: {DateTime.Now} -- Message: ClientId = {context.ClientId}," +
            $" Topic = {context.ApplicationMessage?.Topic}, Payload = {payload}," +
            $" QoS = {context.ApplicationMessage?.QualityOfServiceLevel}," +
            $" Retain-Flag = {context.ApplicationMessage?.Retain}");

        return Task.CompletedTask;
    }


    public void Dispose() => Dispose(true);

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposedValue)
        {
            if (disposing)
            {
                _mqttServer?.Dispose();
            }

            _disposedValue = true;
        }
    }
}
