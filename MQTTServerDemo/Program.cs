using MQTTServerDemo;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddHostedService<Server>();
    })
    .Build();

await host.RunAsync();
