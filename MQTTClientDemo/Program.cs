﻿using MQTTClientDemoPublish;



using var pub = new MQTTPublisher();
await pub.ConnectAsync();

while (true){
    await pub.PublishAsync(DateTimeOffset.Now.ToString());
    await Task.Delay(5000);
}
