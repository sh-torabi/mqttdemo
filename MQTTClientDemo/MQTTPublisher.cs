﻿using MQTTnet;
using MQTTnet.Client;

namespace MQTTClientDemoPublish;

public class MQTTPublisher : IDisposable
{
    private readonly IMqttClient mqttClient;
    private bool _disposedValue;

    public MQTTPublisher()
    {
        var mqttFactory = new MqttFactory();
        mqttClient = mqttFactory.CreateMqttClient();
        mqttClient.ConnectedAsync += OnConnected;
        mqttClient.DisconnectedAsync += OnDisconnected;
    }

    public async Task ConnectAsync()
    {
        var mqttClientOptions = new MqttClientOptionsBuilder()
            .WithClientId("Dev.To")
            .WithTcpServer("localhost", 704)
            .Build();

        await mqttClient.ConnectAsync(mqttClientOptions, CancellationToken.None);
        Console.WriteLine("The MQTT client is connected.");
    }

    public async Task PublishAsync(string message)
    {
        var applicationMessage = new MqttApplicationMessageBuilder()
            .WithTopic("mqttnet/samples/topic/2")
            .WithPayload(message)
            .Build();

        await mqttClient.PublishAsync(applicationMessage, CancellationToken.None);
        Console.WriteLine($"Publish Message{message}");
    }

    public Task OnConnected(MqttClientConnectedEventArgs obj)
    {
        Console.WriteLine("Successfully connected.");
        return Task.CompletedTask;
    }

    public Task OnDisconnected(MqttClientDisconnectedEventArgs obj)
    {
        Console.WriteLine("Successfully disconnected.");
        return Task.CompletedTask;
    }

    public void Dispose() => Dispose(true);

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposedValue)
        {
            if (disposing)
            {
                mqttClient?.Dispose();
            }

            _disposedValue = true;
        }
    }
}
