﻿
using MQTTClientDemoSubscribe;

using var sub = new Subscriber();

await sub.ConnectAsync();
await sub.SubscribeAsync("mqttnet/samples/topic/2");

Console.WriteLine("MQTT client subscribed to topic .");

Console.WriteLine("Press enter to exit.");
Console.ReadLine();