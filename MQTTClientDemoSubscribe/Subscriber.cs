﻿using MQTTnet;
using MQTTnet.Client;
using System.Text;

namespace MQTTClientDemoSubscribe;

public class Subscriber: IDisposable
{
    private readonly IMqttClient mqttClient;
    private bool _disposedValue;

    public Subscriber()
    {
        var mqttFactory = new MqttFactory();
        
        mqttClient = mqttFactory.CreateMqttClient();
        mqttClient.ApplicationMessageReceivedAsync += ApplicationMessageReceivedAsync;
        mqttClient.ConnectedAsync += OnConnected;
        mqttClient.DisconnectedAsync += OnDisconnected;
    }

    public async Task ConnectAsync()
    {
        var mqttClientOptions = new MqttClientOptionsBuilder()
        .WithTcpServer("localhost", 704)
        .Build();

        await mqttClient.ConnectAsync(mqttClientOptions, CancellationToken.None);
    }

    public async Task SubscribeAsync(string topic)
    {
        var mqttSubscribeOptions = new MqttClientSubscribeOptionsBuilder()
            .WithTopicFilter(f => { f.WithTopic(topic); })
            .Build();

        await mqttClient.SubscribeAsync(mqttSubscribeOptions, CancellationToken.None);
    }

    private Task ApplicationMessageReceivedAsync(MqttApplicationMessageReceivedEventArgs context)
    {
        var payload = context.ApplicationMessage?.Payload == null ? null : Encoding.UTF8.GetString(context.ApplicationMessage?.Payload);
        Console.WriteLine($"Received application message.{payload}");

        return Task.CompletedTask;
    }

    public Task OnConnected(MqttClientConnectedEventArgs obj)
    {
        Console.WriteLine("Successfully connected.");
        return Task.CompletedTask;
    }

    public Task OnDisconnected(MqttClientDisconnectedEventArgs obj)
    {
        Console.WriteLine("Successfully disconnected.");
        return Task.CompletedTask;
    }

    public void Dispose() => Dispose(true);

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposedValue)
        {
            if (disposing)
            {
                mqttClient?.Dispose();
            }

            _disposedValue = true;
        }
    }
}
